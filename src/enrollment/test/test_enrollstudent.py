import unittest

from src.enrollment import EnrollStudent

class TestEnrollStudent(unittest.TestCase):

  def setUp(self):
    self.enrollStudent:EnrollStudent = EnrollStudent()
    return super().setUp()
  
  def test_should_not_enroll_without_valid_student_name(self):
    enroll = {
      "student": {
        "name": "Ana"
      }
    }
    with self.assertRaisesRegex(ValueError, 'Invalid name.'):
      self.enrollStudent.execute(enroll)

  def test_should_not_enroll_without_valid_student_cpf(self):
    enroll = {
      "student": {
        "name": "Ana lise",
        "cpf": "123.456.789-99"
      }
    }
    with self.assertRaisesRegex(ValueError, 'Invalid cpf.'):
      self.enrollStudent.execute(enroll)

  def test_should_enroll_valid_student(self):
    enroll = {
      "student": {
        "name": "Ana lise",
        "cpf": "832.081.519-34"
      }
    }
    self.enrollStudent.execute(enroll)
    assert len(self.enrollStudent.enrollments) == 1

  def test_should_enroll_not_duplicated_valid_student(self):
    enrollments = [{
      "student": {
        "name": "Ana lise",
        "cpf": "832.081.519-34"
      }
    },
    {
      "student": {
        "name": "Ana luiza",
        "cpf": "511.029.690-10"
      }
    }]
    for enroll in enrollments:
      self.enrollStudent.execute(enroll)
    assert len(self.enrollStudent.enrollments) == 2

  def test_should_not_enroll_duplicated_student(self):
    enroll = {
      "student": {
        "name": "Ana lise",
        "cpf": "832.081.519-34"
      }
    }
    self.enrollStudent.execute(enroll)
    with self.assertRaisesRegex(ValueError, 'Enrollment with duplicated student is not allowed.'):
      self.enrollStudent.execute(enroll)