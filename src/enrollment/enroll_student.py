from src.validators import Name, Cpf

class EnrollStudent:
  
  def __init__(self):
    self.enrollments:list = []

  def has_duplicated_enrollment(self, valid_cpf:str):
    if next((True for item in self.enrollments if item['student']['cpf'] == valid_cpf), False):
      raise ValueError('Enrollment with duplicated student is not allowed.')

  def execute(self, request_student:dict):
    valid_name:str = Name(request_student['student']['name']).value
    valid_cpf:str = Cpf(request_student['student']['cpf']).value
    enroll = {
      "student": {
        "name": valid_name,
        "cpf": valid_cpf
      }
    }
    if not self.has_duplicated_enrollment(valid_cpf):
      self.enrollments.append(enroll)