import re

class Name:

  value:str = None

  def __init__(self, name:str):
    if not re.match(r'^([A-Za-z]+ )+([A-Za-z])+$', name):
      raise ValueError('Invalid name.')
    self.value = name